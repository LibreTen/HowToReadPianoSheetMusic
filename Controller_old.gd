extends Node2D


var notes_playing = []
var song_time = 0.0
var playing = false

var known_songs = [0,1,2]
var song_list = []
var songs_left = []
var note_count = 0

func _input(event):
	if !event.is_action("modifier"):
		if event.is_action_pressed("note0"):
			play_note(1)
		elif event.is_action_pressed("note1"):
			play_note(2)
		elif event.is_action_pressed("note2"):
			play_note(3)
		elif event.is_action_pressed("note3"):
			play_note(4)
	else:
		if event.is_action_pressed("note0"):
			play_note(5)
		elif event.is_action_pressed("note1"):
			play_note(6)
		elif event.is_action_pressed("note2"):
			play_note(7)
		elif event.is_action_pressed("note3"):
			play_note(8)
	if event.is_action_pressed("note4"):
		play_note(5)
	elif event.is_action_pressed("note5"):
		play_note(6)
	elif event.is_action_pressed("note6"):
		play_note(7)
	elif event.is_action_pressed("note7"):
		play_note(8)

func _ready():
	var file = File.new()
	file.open("res://songs.json", file.READ)
	var text = file.get_as_text()
	song_list = parse_json(file.get_as_text()).songs
	start()
	

func play_note(i):
	
	notes_playing.append(i)
	
	
	#if(!playing):
	#	start()
	note_count +=1
	
	if(check_note(i)):
		start()
		note_count=1
		if !check_note(i):
			note_count=1
		else:
			note_count=0
	get_node("sounds/"+str(i)).play()
	$timer_max_delay.start()

func check_note(i):
	for u in songs_left:
		if(i != u[note_count-1]):
			songs_left.remove(songs_left.find(u))
	
	for u in songs_left:
		if(u.size() == note_count):
			activate_spell(u)
			playing = false
	
	return songs_left.empty()

func start():
	note_count = 0
	playing = true
	songs_left.clear()
	for u in known_songs:
		songs_left.append(song_list[u])


func activate_spell(u):
	var id : int = song_list.find(u)
	if(id ==0):
		print("haha")
	if(id==1):
		print("b")
	if(id==2):
		print("c")
	start()

# Called when the node enters the scene tree for the first time.



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$Debug/notes.text =str(songs_left)#str(playing)+ str(notes_playing)


func _on_timer_max_delay_timeout():
	playing = false
