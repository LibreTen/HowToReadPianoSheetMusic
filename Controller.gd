extends Node2D

const bgm_loop = preload("res://sfx/not_so_creepyl.wav")

var file_dummy = "{\"songs\":[[1,3,8],[8,1],[4,2,6],[8,8,5,6,0,8,0,7],[4,5,8,1],[6,8,0,3]]}"

var notes_playing = []
var song_time = 0.0
var playing = false

var known_songs = []
var song_list = []
var songs_left = []
var dead := false
var checkpoint_pos:Vector2
var former_checkpoint

func _input(event):
	if dead:
		return
	if !Input.get_action_strength("modifier"):
		if event.is_action_pressed("note0"):
			play_note(1)
		elif event.is_action_pressed("note1"):
			play_note(2)
		elif event.is_action_pressed("note2"):
			play_note(3)
		elif event.is_action_pressed("note3"):
			play_note(4)
	else:
		if event.is_action_pressed("note0"):
			play_note(5)
		elif event.is_action_pressed("note1"):
			play_note(6)
		elif event.is_action_pressed("note2"):
			play_note(7)
		elif event.is_action_pressed("note3"):
			play_note(8)
	if event.is_action_pressed("note4"):
		play_note(5)
	elif event.is_action_pressed("note5"):
		play_note(6)
	elif event.is_action_pressed("note6"):
		play_note(7)
	elif event.is_action_pressed("note7"):
		play_note(8)
	
	elif event.is_action_pressed("cancel_song"):
		start()

func _ready():
	#var file = File.new()
	#file.open("res://songs.json", file.READ)
	#var text = file.get_as_text()
	song_list = parse_json(file_dummy).songs
	start()
	checkpoint_pos = $Player.position

func play_note(i):
	$timer_max_delay.start()
	if(notes_playing.size() == 8):
		start()
		if(i!=0):
			$timer_max_delay.start()
	
	if(notes_playing.size()==7):
		$timer_max_delay.stop()
	
	notes_playing.append(i)
	
	
	#visual effects
	get_node("sounds/"+str(i)).play()
	get_node("HUD/Sheet/"+str(notes_playing.size()-1)).show()
	get_node("HUD/Sheet/"+str(notes_playing.size()-1)).frame = i
	check_note()

func check_note():
	for u in songs_left:
		if(notes_playing.size()==u.size()):
			var equal = true
			for i in notes_playing.size():
				if(notes_playing[i] != u[i]):
					equal = false
					break
			if(equal):
				activate_spell(u)


func start():
	songs_left.clear()
	notes_playing.clear()
	$timer_max_delay.stop()
	for u in known_songs:
		songs_left.append(song_list[u])
	
	for u in get_node("HUD/Sheet").get_children():
		u.hide()


func activate_spell(u):
	var id : int = song_list.find(u)
	if(id ==0): #highest jump
		$Player.highest_jump()
	if(id==1): #extra jump
		$Player.extra_jump()
	if(id==2):
		$Player.long_jump()
	if(id==3):
		$Player.flip_jump()
	if(id==4):
		$Player.slow_mo()
	if id==5:
		$Player.shockwave()
	start()


func game_over(body):
	if(body!=$Player):
		return
	$sounds/die.play()
	$ResetTimer.start()
	$Player.dead = true
	dead = true
	start()



func _on_timer_max_delay_timeout():
	play_note(0)


func _on_ResetTimer_timeout():
	$Player.position=checkpoint_pos
	$Player.dead = false
	$Player/AnimatedSprite.flip_v = false
	dead = false

func checkpoint(body,cp):
	if body!=$Player || cp==former_checkpoint:
		return
	$sounds/checkpoint.play()
	cp.get_node("Sprite").frame=1
	if former_checkpoint!= null:
		former_checkpoint.get_node("Sprite").frame=0
	former_checkpoint = cp
	checkpoint_pos=cp.position

func pickup(body,id):
	if body!=$Player:
		return
	$sounds/powerup.play()
	known_songs.append(id)
	get_node("HUD/Pause/TextureRect/"+str(id)).show()
	get_node("HUD/Pause/Hints/"+str(id)).show()
	start()


func _on_End_body_entered(body):
	if body!=$Player:
		return
	if known_songs.size()!=6:
		get_tree().change_scene("res://BadEnd.tscn")
	else:
		get_tree().change_scene("res://NormalEnd.tscn")


func _on_bgm_finished():
	$sounds/bgm.stream = bgm_loop
	$sounds/bgm.play()
