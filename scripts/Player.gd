extends KinematicBody2D


# Declare member variables here. Examples:
const MAX_JUMPS = 1

export(Curve) var time_growth : Curve

var walk_speed := 6000
var mov := Vector2.ZERO
var move := Vector2.ZERO
var slide = 1
var jumps := 0
var time_factor:=1.0

var jump_force = -10000
var fall_speed = 20000

var animation := 0
var color:Color = Color.orange

var dead := false

var side := true

var high_jumping = false
var extra_jumping = false

var gravity_dir = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	mov.x = Input.get_action_strength("ui_right")-Input.get_action_strength("ui_left")
	mov.x *= walk_speed
	
	if dead:
		$AnimatedSprite.flip_v = true
		mov = Vector2.ZERO
		move_and_slide(Vector2.DOWN * 200)
		jumps = 2
	
	
	if(!is_on_floor()):
		set_animation(2)
	elif(mov.x != 0):
		set_animation(1)
	else:
		set_animation(0)
	
	$AnimatedSprite.speed_scale=time_factor*1.5
	
	if(side && mov.x<0):
		side = !side
		$AnimatedSprite.flip_h = !$AnimatedSprite.flip_h
	elif(!side && mov.x>0):
		side = !side
		$AnimatedSprite.flip_h = !$AnimatedSprite.flip_h
	
	if(is_on_floor()):
		jumps = 0
		mov.y = 1000#30/time_factor
	else:
		mov.y += fall_speed*delta*time_factor
	
	if(Input.is_action_just_pressed("ui_accept") && jumps <MAX_JUMPS):
		#$sound_jump.play()
		jumps+=1
		mov.y = jump_force
		get_node("../sounds/jump").play()
	
	if high_jumping:
		if(jumps <MAX_JUMPS):
			#$sound_jump.play()
			jumps+=1
			mov.y = jump_force*1.5
			get_node("../sounds/jumphigh").play()
		high_jumping=false
	
	if extra_jumping:
		if(jumps <MAX_JUMPS+1):
			#$sound_jump.play()
			jumps+=1
			mov.y = jump_force
			get_node("../sounds/jump").play()
		extra_jumping=false
	
	move.y=mov.y*gravity_dir
	slide = slide + (1 - slide) * 0.1*delta
	
	time_factor = time_growth.interpolate((10-$Slow_mo_timer.time_left)/10)
	move = move.linear_interpolate(Vector2(mov.x,move.y),slide*time_factor)
	var velocity=move_and_slide(move*delta*time_factor,Vector2.UP*gravity_dir)
	
	#var velocity = move_and_slide(mov*delta,Vector2.UP)
	
#	if(velocity.y == 0):
#		mov.y = 20
	

func highest_jump():
	high_jumping = true

func extra_jump():
	extra_jumping = true

func long_jump():
	move.x += 70000*(int(!$AnimatedSprite.flip_h)-0.5)*2#/time_factor
	slide = 0.1
	get_node("../sounds/long").play()

func flip_jump():
	if is_on_floor():
		gravity_dir *=-1
		$AnimatedSprite.flip_v = !$AnimatedSprite.flip_v

func slow_mo():
	$Slow_mo_timer.start()

func shockwave():
	get_node("../sounds/shockwave").play()
	for u in get_node("../Destroyables").get_children():
		if (u.position-position).length() <= 40:
			u.queue_free()

func set_animation(anim : int):
	if(anim != animation):
		animation = anim
		match anim:
			0:$AnimatedSprite.play("default")
			1:$AnimatedSprite.play("run")
			2:$AnimatedSprite.play("jump")


