extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _input(event):
	if event.is_action_pressed("cancel_song") && get_tree().paused:
		$Hints.visible = !$Hints.visible
	
	if event.is_action_pressed("ui_accept") && get_tree().paused:
		$Instructions.visible=!$Instructions.visible
		$TextureRect.visible = !$TextureRect.visible
		
	if event.is_action_pressed("fullscreen") && get_tree().paused:
		OS.window_fullscreen = !OS.window_fullscreen
	if event.is_action_pressed("start"):
		get_tree().paused = !get_tree().paused
		visible = !visible
		if visible:
			get_node("../../sounds/bgm").volume_db = -10
		else:
			get_node("../../sounds/bgm").volume_db = -2
