extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)


func _input(event):
#	if event.is_action_pressed("ui_accept"):
#		$Instructions.visible=!$Instructions.visible
#		$Start.visible = !$Start.visible
	if event.is_action_pressed("start"):
		get_tree().change_scene("res://Level.tscn")
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()
	if event.is_action_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
